<?php

namespace Drupal\flexi_gallery\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;

/**
 * @FieldFormatter(
 *   id = "flexi_gallery",
 *   label = @Translation("Flexi Gallery"),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class FlexiGalleryFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'show_big_images' => TRUE,
      'big_image_style' => '',
      'big_image_link_to_original' => TRUE,
      'original_image_style' => '',
      'big_image_link_colorbox' => FALSE,

      'show_small_images' => TRUE,
      'small_image_style' => '',
      'small_images_visible_count' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $field_name = $this->fieldDefinition->getName();
    $image_styles = image_style_options();
    $formatter_settings = $this->getSettings();

    $element['show_big_images'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show big image'),
      '#default_value' => $formatter_settings['show_big_images'],
    ];

    $element['big_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Big image style'),
      '#options' => $image_styles,
      '#default_value' => $formatter_settings['big_image_style'],
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $field_name . '][settings_edit_form][settings][show_big_images]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $element['big_image_link_to_original'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Wrap big image with link to original image'),
      '#default_value' => $formatter_settings['big_image_link_to_original'],
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $field_name . '][settings_edit_form][settings][show_big_images]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $element['original_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Original image style'),
      '#options' => $image_styles,
      '#default_value' => $formatter_settings['original_image_style'],
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $field_name . '][settings_edit_form][settings][big_image_link_to_original]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $element['big_image_link_colorbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open original image in colorbox'),
      '#default_value' => $formatter_settings['big_image_link_colorbox'],
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $field_name . '][settings_edit_form][settings][big_image_link_to_original]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $element['show_small_images'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show small images'),
      '#default_value' => $formatter_settings['show_small_images'],
    ];

    $element['small_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Small image style'),
      '#options' => $image_styles,
      '#default_value' => $formatter_settings['small_image_style'],
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $field_name . '][settings_edit_form][settings][show_small_images]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $element['small_images_visible_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of visible small images'),
      '#default_value' => $formatter_settings['small_images_visible_count'],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    if ($items->isEmpty()) {
      return [];
    }

    $formatter_settings = $this->getSettings();
    $elements = [
      '#all_in_one' => TRUE,
    ];

    $images = [];

    // Set default image
    if ($items->isEmpty()) {
      // @see \Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase::getEntitiesToView()
      $default_image = $items->getSetting('default_image');
      if (empty($default_image['uuid']) && $items->getFieldDefinition() instanceof FieldConfigInterface) {
        $default_image = $items->getFieldDefinition()->getFieldStorageDefinition()->getSetting('default_image');
      }
      if (!empty($default_image['uuid']) && $file = \Drupal::service('entity.repository')->loadEntityByUuid('file', $default_image['uuid'])) {
        $items = clone $items;
        $items->setValue([
          'target_id' => $file->id(),
          'alt' => $default_image['alt'],
          'title' => $default_image['title'],
          'width' => $default_image['width'],
          'height' => $default_image['height'],
          'entity' => $file,
          '_loaded' => TRUE,
          '_is_default' => TRUE,
        ]);
      }
    }

    foreach ($items as $item) {
      $images[] = [
        'uri' => $item->entity->uri->value,
        'width' => $item->width,
        'height' => $item->height,
        'title' => $item->title,
        'alt' => $item->alt,
      ];
    }

    $elements[] = [
      '#theme' => 'flexi_gallery',
      '#images' => $images,

      '#show_big_images' => $formatter_settings['show_big_images'],
      '#big_image_style' => $formatter_settings['big_image_style'],
      '#big_image_link_to_original' => $formatter_settings['big_image_link_to_original'],
      '#original_image_style' => $formatter_settings['original_image_style'],
      '#big_image_link_colorbox' => $formatter_settings['big_image_link_colorbox'],

      '#show_small_images' => $formatter_settings['show_small_images'],
      '#small_image_style' => $formatter_settings['small_image_style'],
      '#small_images_visible_count' => $formatter_settings['small_images_visible_count'],

      '#context' => [
        'from' => 'formatter',
        'field_name' => $items->getName(),
        'entity' => $items->getEntity(),
        'view_mode' => $this->viewMode,
      ],
    ];

    return $elements;
  }

}
