(function ($) {
  Drupal.behaviors.flexiGallery = {
    attach: function (context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      once('flexi-gallery', '.flexi-gallery', context).forEach(function (element) {
        var $flexiGalleryElements = $(element).addClass('flexi-gallery--processed');

        $flexiGalleryElements.each(function () {
          var $flexiGallery = $(this);
          var $bigImagesWrapper = $flexiGallery.find('.flexi-gallery__big-images');
          var $smallImagesWrapper = $flexiGallery.find('.flexi-gallery__small-images');

          if ($bigImagesWrapper.length) {
            $flexiGallery.find('.flexi-gallery__small-item').on('click.flexi-gallery', function (event) {
              var $smallItem = $(this);
              var index = $flexiGallery.find('.flexi-gallery__small-item').index($smallItem);

              // Remove "active" class
              $bigImagesWrapper.find('.flexi-gallery__big-item--active').removeClass('flexi-gallery__big-item--active');
              $smallImagesWrapper.find('.flexi-gallery__small-item--active').removeClass('flexi-gallery__small-item--active');

              // Add "active" class
              $bigImagesWrapper.find('.flexi-gallery__big-item').eq(index).addClass('flexi-gallery__big-item--active');
              $smallItem.addClass('flexi-gallery__small-item--active');

              event.preventDefault();
            });

            // Select first image on first load
            if ($bigImagesWrapper.length && $smallImagesWrapper.find('.flexi-gallery__small-item--active').length == 0) {
              $smallImagesWrapper.find('.flexi-gallery__small-item:first').click();
            }
          }
        });
      });
    }
  };
})(jQuery);
